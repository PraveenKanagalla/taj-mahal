function openNav() {
    document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
    document.getElementById("myNav").style.width = "0%";
}

var myIndex = 0;

function itemsCarousel() {
    var j;
    var imgResponsive = document.getElementsByClassName('img-responsive');
    for (j = 0; j < imgResponsive.length; j++) {
        imgResponsive[j].style.display = 'none';
    }
    myIndex++;
    if (myIndex > imgResponsive.length) {
        myIndex = 1;
    }
    imgResponsive[myIndex - 1].style.display = "block";
    setTimeout(itemsCarousel, 3000); // Change image every 3s seconds
}
itemsCarousel();

var slideIndex = 0;

function indiaCarousel() {
    var i;
    var mySlides = document.getElementsByClassName('mySlides');
    for (i = 0; i < mySlides.length; i++) {
        mySlides[i].style.display = 'none';
    }
    slideIndex++;
    if (slideIndex > mySlides.length) {
        slideIndex = 1
    }
    mySlides[slideIndex - 1].style.display = "block";
    setTimeout(indiaCarousel, 1000); // Change image every 1 seconds
}
indiaCarousel();

function myMap() {
    var mapProp = {
        center: new google.maps.LatLng(51.508742, -0.120850),
        zoom: 5,
    };
    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
}
